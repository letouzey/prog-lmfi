Ancien dépôt du cours de programmation fonctionnelle en Coq (M2 LMFI, Université de Paris)
==========================================================================================

**Les fichiers sont [maintenant ici](https://gitlab.math.univ-paris-diderot.fr/letouzey/coq-lmfi/tree/master/1-prog)** dans un dépôt regroupant les deux parties du cours (programmation et preuves).

Il ne reste ici que le sous-répertoire `ocaml` contenant:
 - les notes des séances de pré-rentrée (initiation à la programmation OCaml)
 - des feuilles de TD en OCaml pour l'ancienne mouture de ce module (Modèles de la Programmation, en OCaml)

