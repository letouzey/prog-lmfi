TD2 : Paresse
=============

**M2 LMFI**

On considère la mini-bibliothèque suivante de flots infinis (ou *streams*) :

```ocaml
exception Empty

type 'a stream = ('a cell) Lazy.t
and 'a cell = Cons of 'a * 'a stream
let empty : 'a stream = lazy (raise Empty)
let cons x s = lazy (Cons (x,s))
let get s = match s with lazy (Cons (x,s')) -> (x,s')
```

Notez l'absence du constructeur `Nil` : la paresse permet de le remplacer par une exception !

#### Exercice 1

  1. Écrire une fonction `of_list : 'a list -> 'a MyStream.t` utilisant `List.fold_right`.
  2. Écrire une fonction `take : int -> 'a stream -> 'a list` donnant les `n` premiers éléments d'une stream, ou bien tous les éléments s'il y en a moins que `n`. Tester à l'aide de la fonction précédente.
  3. Écrire une fonction `from : int -> int stream` donnant une stream de tous les entiers successifs à partir de `n` inclus: `n`, `n+1`, `n+2`, etc. Tester avec la fonction `take`.


#### Exercice 2

Le *crible d'Ératosthène* est une méthode pour calculer la séquence des nombres premiers:

  - Un générateur engendre le flot de nombres à partir de 2.
  - La sortie du générateur passe par une chaîne de filtres, dont  chacun supprime tous les éléments qui sont divisible par un certain nombre.

![Portrait of Eratosthenes](https://upload.wikimedia.org/wikipedia/commons/a/a2/Portrait_of_Eratosthenes.png)

Le crible fonctionne comme suit :

  - Il laisse passer le premier élément (appelé `x`).
  - Pour tous les éléments suivants de l'entrée il élimine ceux qui sont multiples de `x`. Le flot des nombres qui restent après cette élimination est passé à une nouvelle instance du crible.

  1. Écrire une fonction `no_multiple n s` qui produit une nouvelle \emph{stream} à partir de la stream `s` en enlevant tous les multiples de `n`. En supposant que `s` est triée dans l'ordre croissant, essayer de coder cette fonction sans utiliser de multiplication ou division.
  2. Écrire une fonction `sieve` produisant le flot infini des nombres premiers par la méthode d'Eratosthène. En afficher les 1000 premiers.

